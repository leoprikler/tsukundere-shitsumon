#
# Automatically generated <>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: \"PACKAGE VERSION\"\n"
"Report-Msgid-Bugs-To: nobody <nobody@home>\n"
"POT-Creation-Date: 2021-06-08 21:44:14+0200\n"
"PO-Revision-Date: 2021-06-24 13:15+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 3.36.0\n"

#: shitsumon.w:68
msgid "Aoi"
msgstr "Aoi"

#: shitsumon.w:69
msgid "Midori"
msgstr "Midori"

#: shitsumon.w:105
msgid "Me"
msgstr "Ich"

#: shitsumon.w:111
msgid ""
"It's only when I hear the sounds of shuffling feet and\n"
"supplies being put away that I realize that the lecture's over."
msgstr ""
"Erst nachdem ich den Klang unruhiger Füße und weggelegter\n"
"Unterrichtsmaterialien höre, bemerke ich, dass der Unterricht\n"
"vorbei ist."

#: shitsumon.w:113
msgid ""
"It's not like I wanted to listen to some old white guy talking\n"
"about some dead old white guy."
msgstr ""
"Es ist nicht so, dass ich diesem alten Mann bei seinen\n"
"Erzählungen über tote Männer so unbedingt zuhören wollte."

#: shitsumon.w:115
msgid "Although my thoughts themselves are pure…"
msgstr "Doch meine Gedanken selbst sind rein…"

#: shitsumon.w:116
msgid " at least I hope so."
msgstr " denke ich zumindest."

#: shitsumon.w:120
msgid "The truth however is, that I'm a hopeless case."
msgstr "Die Wahrheit ist wohl eher, dass ich hoffnungslos verloren bin."

#: shitsumon.w:124
msgid "I've always been admiring Midori, ever since our childhood."
msgstr "Seit unserer Kindheit verehre ich Midori."

#: shitsumon.w:128
msgid "Sounds like a pattern straight out of an anime, doesn't it?"
msgstr "Hört sich an wie ein Klischee direkt aus 'nem Anime, ne?"

#: shitsumon.w:132
msgid "Well it's true."
msgstr "Aber es ist wahr."

#: shitsumon.w:133
msgid "Good job, Acchan."
msgstr "Gute Arbeit, Acchan."

#: shitsumon.w:134
msgid ""
"Even now, my mind is playing tricks on me.\n"
"There's no way Midori would greet me so casually."
msgstr ""
"Selbst jetzt spielt mir mein Geist Streiche.\n"
"Es ist unmöglich, dass Midori mich einfach so grüßt."

#: shitsumon.w:138
msgid "Is this the real life?"
msgstr "Ist das das richtige Leben?"

#: shitsumon.w:141
msgid "Yes"
msgstr "Ja"

#: shitsumon.w:140
msgid "No, it's just fantasy."
msgstr "Nein, es ist bloße Fantasie"

#: shitsumon.w:143
msgid "Yes, I thought so."
msgstr "Dacht' ich's mir."

#: shitsumon.w:147
msgid "Wait, it is?"
msgstr "Was, ist es—?"

#: shitsumon.w:148
msgid "What?"
msgstr "Was?"

#: shitsumon.w:149
msgid "Oh no, I just said that out loud."
msgstr "Oh nein, das habe ich gerade laut gesagt."

#: shitsumon.w:150
msgid ""
"At least I didn't accidentally confess my feelings for her or\n"
"something like that."
msgstr ""
"Wenigstens habe ich nicht versehentlich meine Gefühle für sie\n"
"gestanden oder etwas in die Richtung."

#: shitsumon.w:154
msgid "Speaking about confessing…"
msgstr "Wo wir schon von Gefühlen reden…"

#: shitsumon.w:157
msgid "Do it!"
msgstr "Tu es!"

#: shitsumon.w:156
msgid "Nah, let's chill."
msgstr "Nö, lass ma."

#: shitsumon.w:158
msgid "You do not have enough courage to pick this option."
msgstr "Du hast nicht genügend Mut, diese Option zu wählen."

#: shitsumon.w:159
msgid "Oh… uh… nothing."
msgstr "Öh… äh… nichts."

#: shitsumon.w:160
msgid "Ahh… nothing."
msgstr "Ähm… nichts."

#: shitsumon.w:161
msgid "Would nothing really occupy your mind so long?"
msgstr "Würde nichts dich wirklich so lange beschäftigen?"

#: shitsumon.w:162
msgid "Then again, you're Acchan."
msgstr "Vielleicht doch, immerhin bist du Acchan."

#: shitsumon.w:163
msgid "W-what's that supposed to mean?"
msgstr "W-was soll das denn heißen?"

#: shitsumon.w:164
msgid "Nothing."
msgstr "Nichts."

#: shitsumon.w:165
msgid "S-so, you're heading home now, right?"
msgstr "S-so, du gehst jetzt nach Hause, oder?"

#: shitsumon.w:166
msgid "What? Oh, yeah."
msgstr "Was? Oh, ja."

#: shitsumon.w:167
msgid "Care to join me?"
msgstr "Willst du mich begleiten?"

#: shitsumon.w:168
msgid "Sure!"
msgstr "Sicher!"

#: shitsumon.w:171
msgid ""
"After a somewhat convenient transition, we reach the meadows\n"
"just outside the neighborhood where we both live."
msgstr ""
"Nach einem ziemlich praktischen Übergang erreichen wir Felder\n"
"nicht weit entfernt von der Nachbarschaft, in der wir beide\n"
"leben."

#: shitsumon.w:173
msgid "Ahh, this brings back memories."
msgstr "Ahh, das weckt Erinnerungen."

#: shitsumon.w:174
msgid "We used to play a lot out here as children, didn't we?"
msgstr "Hier haben wir als Kinder oft gespielt, nicht wahr?"

#: shitsumon.w:175
msgid "Indeed, we did… on our smartphones, though."
msgstr "In der Tat, das taten wir… auf unseren Smartphones."

#: shitsumon.w:176
msgid "I can't even remember if I cared about scenery back then."
msgstr ""
"Ich kann mich nicht einmal daran erinnern, ob ich damals so\n"
"viel über Landschaften nachdachte."

#: shitsumon.w:177
msgid "Even if I did, it's not like locations matter to me now."
msgstr "Selbst wenn, so bedeuten mir Orte jetzt recht wenig."

#: shitsumon.w:178
msgid "What matters, is…"
msgstr "Bedeutung hat…"

#: shitsumon.w:179
msgid "Uhm…"
msgstr "Ähm…"

#: shitsumon.w:180
msgid "Yes?"
msgstr "Ja?"

#: shitsumon.w:181
msgid ""
"That smile is where my problems start, but certainly not\n"
"where they end."
msgstr ""
"Dieses Lächeln war mein erstes Problem, aber bei Gott nicht das\n"
"letzte."

#: shitsumon.w:183
msgid "If only I could muster up some courage and be done with it."
msgstr "Wenn ich doch bloß ein wenig Mut aufbringen könnte…"

#: shitsumon.w:185
msgid "But I decided, that I would."
msgstr "Aber ich habe mich entschieden."

#: shitsumon.w:187
msgid "Not that I ever could."
msgstr "Nicht, dass ich das je könnte."

#: shitsumon.w:188
msgid "Nonono, this ends now!"
msgstr "Nein, nein, nein, das endet jetzt!"

#: shitsumon.w:189
msgid "I will ask her right here right now!"
msgstr "Ich frage sie genau hier zu dieser Zeit!"

#: shitsumon.w:190
msgid "Will I…?"
msgstr "Werde ich…?"

#: shitsumon.w:191
msgid "Will you… "
msgstr "Wirst du… "

#: shitsumon.w:194 shitsumon.w:200
msgid "write a Visual Novel with me?"
msgstr "eine Visual Novel mit mir schreiben?"

#: shitsumon.w:196 shitsumon.w:223
msgid "visit tomorrow's lecture?"
msgstr "die morgige Vorlesung besuchen?"

#: shitsumon.w:197 shitsumon.w:254
msgid "walk all the way to my house?"
msgstr "ganz bis zu meinem Haus gehen?"

#: shitsumon.w:198
msgid "gain consciousness and take over my PC?"
msgstr "Bewusstsein erlangen und meinen PC übernehmen?"

#: shitsumon.w:201
msgid "Sure."
msgstr "Sicher."

#: shitsumon.w:202
msgid "But… "
msgstr "Aber…"

#: shitsumon.w:203
msgid "what exactly is a Visual Novel to you?"
msgstr "was genau ist eine Visual Novel für dich?"

#: shitsumon.w:206
msgid "A book."
msgstr "Ein Buch"

#: shitsumon.w:208
msgid "A video game."
msgstr "Ein Spiel"

#: shitsumon.w:209
msgid "Actually, I usually read the dirty ones…"
msgstr "Eigentlich lese ich ja nur die dreckigen…"

#: shitsumon.w:210
msgid "Is that so?"
msgstr "Ist das so?"

#: shitsumon.w:212
msgid "Aren't you an honest one?"
msgstr "Bist du nicht ehrlich?"

#: shitsumon.w:215
msgid "Well, now that that's cleared up, let's get to writing."
msgstr "Nun gut, jetzt wo das geklärt ist, lass uns schreiben."

#: shitsumon.w:215
msgid ""
"Midori certainly seems more enthusiastic about this than you\n"
"anticipated."
msgstr ""
"Midori geht die Sache mit etwas mehr Elan an als du gedacht\n"
"hättest."

#: shitsumon.w:217
msgid "Wait, why is this in second person now?"
msgstr "Warte, warum ist das jetzt in der zweiten Person?"

#: shitsumon.w:218
msgid "Ahem, let's get back to the story."
msgstr "Ähem, zurück zur Geschichte."

#: shitsumon.w:219
msgid "Yes!!!"
msgstr "Ja!!!"

#: shitsumon.w:224
msgid "Which lecture?"
msgstr "Welche Vorlesung?"

#: shitsumon.w:225
msgid "Uhm… \"Depictions of marine life in 21st century artwork\"?"
msgstr ""
"Ähm… \"Darstellungen mariner Lebensformen des einundzwanzigsten\n"
"Jahrhunderts\"?"

#: shitsumon.w:226
msgid "Sure, why not?"
msgstr "Sicher, wieso nicht?"

#: shitsumon.w:227
msgid "This would be a good point for a transition."
msgstr "Hier wäre ein guter Zeitpunkt für eine Überleitung."

#: shitsumon.w:229
msgid "Well, beggars can't be choosers."
msgstr "Naja, Bettler können nicht wählerisch sein."

#: shitsumon.w:232
msgid "Oh look at that, they really have a nice selection."
msgstr "Oh schau, die haben wirklich eine gute Auswahl."

#: shitsumon.w:232
msgid ""
"Why do all of these feel like the kind I wouldn't want to\n"
"tell my parents about?"
msgstr ""
"Warum sehen die so aus, als würde ich meinen Eltern\n"
"lieber nicht von ihnen erzählen?"

#: shitsumon.w:234
msgid "That one was certainly inspired by Hokusai."
msgstr "Das hier war bestimmt von Hokusai inspiriert."

#: shitsumon.w:238
msgid "We are always inspired by those, who lived before us."
msgstr "Wir alle sind inspiriert von unseren Vorgängern."

#: shitsumon.w:244
msgid "Making the octopus female… making the wife a husband…\n"
msgstr "Ein weiblicher Oktopus… ein Gatte statt einer Gattin…\n"

#: shitsumon.w:245
msgid ""
"Those ideas only make sense, because they're in reference to\n"
"something else."
msgstr "Diese Ideen ergeben nur Sinn, weil sie andere referenzieren."

#: shitsumon.w:247
msgid "Onee-chan, what are you doing here?"
msgstr "Onee-chan, was machst du hier?"

#: shitsumon.w:248
msgid "I'm the one giving the lecture."
msgstr "Ich halte diese Vorlesung."

#: shitsumon.w:249
msgid ""
"Now take a seat and get ready.\n"
"I need to teach you young, inexperienced kids a lot."
msgstr ""
"Nun setzt euch hin und macht euch bereit.\n"
"Euch jungen, unerfahrenen Kindern muss ich noch viel\n"
"beibringen."

#: shitsumon.w:255
msgid "Wow, you've become bold."
msgstr "Wow, du bist ja auf einmal direkt."

#: shitsumon.w:256
msgid "But sure, why not?"
msgstr "Aber sicher, wieso nicht?"

#: shitsumon.w:257
msgid "It is implied, that she walks you home.\n"
msgstr "Es wird impliziert, dass sie dich nach Hause begleitet.\n"

#: shitsumon.w:258
msgid "But nothing else happens."
msgstr "Aber sonst passiert nichts."

#: shitsumon.w:262
msgid "I'm afraid I can't let you ask that."
msgstr ""
"Es tut mir leid, aber das darf ich dich nicht fragen\n"
"lassen."
