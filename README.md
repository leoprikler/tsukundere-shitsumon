Shitsumon
=========

A reimagination of Ren'py's "The Question" example, with a few twists.

Requirements
------------

- Tsukundere 0.4.0 or later
- Wisp 1.0 or later (tested against 1.0.3+)

When using the Guix recipe for Tsukundere, make sure, that Wisp is one of its
inputs, as it uses static load paths.

Running
-------

Assuming tsukundere lies in your `$PATH` and a fresh checkout of Shitsumon
to be your current working directory, simply run:

    tsukundere run -L $PWD -A $PWD -m shitsumon

If not, adapt the paths accordingly.

To run a translated version of Shitsumon (e.g. the German translation included
in this project) do the following:

    mkdir -p mo/$LANG/LC_MESSAGES
    msgfmt po/$LANG.po -o mo/$LANG/LC_MESSAGES/shitsumon.mo

Then run tsukundere as above, but with `--localedir=$PWD/mo` added to the command
line.

License
-------

The script for this game is licensed under the
[Expat license](https://directory.fsf.org/wiki/License:Expat).  
With the exception of the fonts, all assets have been taken from the
[Ren'py](https://renpy.org) example game "The Question" and should
therefore be under an Expat license as well.
The following credits apply to them:

    Character Art: Deji.
    Original Character Art: derik.

    Background Art: Mugenjohncel.
    Original Background Art: DaFool

    Music By: Alessio

The fonts included in this repo are licensed under the
[SIL OFL 1.1](https://directory.fsf.org/wiki/License:OFL-1.1).

This game uses the Guile Scheme implementation, Guile-SDL2 and Tsukundere,
which are licensed under the GNU Lesser General Public License (GNU LGPL).
You should have obtained a copy of the GNU LGPL along with a distribution
of those packages.  
This game is written in [Wisp](https://www.draketo.de/software/wisp), a
white-space based Scheme dialect,
see also [SRFI 119](https://srfi.schemers.org/srfi-119/srfi-119.html).

<!--
  Don't forget to run the following before saving:
  (setq-local before-save-hook (remq 'delete-trailing-whitespace before-save-hook))
  -->
